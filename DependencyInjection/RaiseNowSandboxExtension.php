<?php

namespace RaiseNow\SandboxBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\DependencyInjection\DefinitionDecorator;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class RaiseNowSandboxExtension extends Extension
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\XmlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.xml');

        foreach ($config['sandboxes'] as $name => $sandboxConfig) {
            $this->setupSandbox($name, $sandboxConfig, $container);
        }

    }


    private function setupSandbox($name, $sandboxConfig, $container) {
        $sandboxId = 'raise_now_sandbox.sandbox.'.$name;
        $container->setDefinition($sandboxId, new DefinitionDecorator('raise_now_sandbox.sandbox.default'));
        $sandbox = $container->getDefinition($sandboxId);
        $sandbox->setPublic(true);
        foreach ($sandboxConfig as $configName => $value) {
            switch ($configName){
                case 'allow_functions':
                case 'allow_closures':
                case 'allow_variables':
                case 'allow_static_variables':
                case 'allow_objects':
                case 'allow_constants':
                case 'allow_globals':
                case 'allow_namespaces':
                case 'allow_aliases':
                case 'allow_classes':
                case 'allow_interfaces':
                case 'allow_traits':
                case 'allow_generators':
                case 'allow_escaping':
                case 'allow_casting':
                case 'allow_error_suppressing':
                case 'allow_references':
                case 'allow_backticks':
                case 'allow_halting':
                    $sandbox->addMethodCall('set_option',array($configName, $value));
                    break;
                case 'whitelisted_functions':
                    $sandbox->addMethodCall('whitelist_func',$value);
                    break;
                case 'whitelisted_variables':
                    $sandbox->addMethodCall('whitelist_var',$value);
                    break;
                case 'whitelisted_globals':
                    $sandbox->addMethodCall('whitelist_global',$value);
                    break;
                case 'whitelisted_super_globals':
                    $sandbox->addMethodCall('whitelist_superglobal',$value);
                    break;
                case 'whitelisted_constants':
                    $sandbox->addMethodCall('whitelist_const',$value);
                    break;
                case 'whitelisted_magic_constants':
                    $sandbox->addMethodCall('whitelist_magic_const',$value);
                    break;
                case 'whitelisted_namespaces':
                    $sandbox->addMethodCall('whitelist_namespace',$value);
                    break;
                case 'whitelisted_aliases':
                    $sandbox->addMethodCall('whitelist_alias',$value);
                    break;
                case 'whitelisted_uses':
                    $sandbox->addMethodCall('whitelist_use',$value);
                    break;
                case 'whitelisted_interfaces':
                    $sandbox->addMethodCall('whitelist_interface',$value);
                    break;
                case 'whitelisted_traits':
                    $sandbox->addMethodCall('whitelist_trait',$value);
                    break;
                case 'whitelisted_keywords':
                    $sandbox->addMethodCall('whitelist_keyword',$value);
                    break;
                case 'whitelisted_types':
                    $sandbox->addMethodCall('whitelist_type',$value);
                    break;
                case 'whitelisted_primitives':
                    $sandbox->addMethodCall('whitelist_primitive',$value);
                    break;
                case 'whitelisted_operators':
                    $sandbox->addMethodCall('whitelist_operator',$value);
                    break;
                case 'whitelisted_classes':
                    $sandbox->addMethodCall('whitelist_class',$value);
                    break;
            }

        }
    }
}
