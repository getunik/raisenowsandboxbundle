<?php

namespace RaiseNow\SandboxBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('raise_now_sandbox');

        $rootNode
            ->children()
                ->arrayNode('sandboxes')
                    ->useAttributeAsKey('name')
                    ->prototype('array')
                        ->treatNullLike(array())
                        ->children()
                            ->scalarNode('allow_closures')->defaultValue(false)->end()
                            ->scalarNode('allow_functions')->defaultValue(false)->end()
                            ->scalarNode('allow_variables')->defaultValue(false)->end()
                            ->scalarNode('allow_static_variables')->defaultValue(false)->end()
                            ->scalarNode('allow_objects')->defaultValue(false)->end()
                            ->scalarNode('allow_constants')->defaultValue(false)->end()
                            ->scalarNode('allow_globals')->defaultValue(false)->end()
                            ->scalarNode('allow_namespaces')->defaultValue(false)->end()
                            ->scalarNode('allow_classes')->defaultValue(false)->end()
                            ->scalarNode('allow_interfaces')->defaultValue(false)->end()
                            ->scalarNode('allow_traits')->defaultValue(false)->end()
                            ->scalarNode('allow_escaping')->defaultValue(false)->end()
                            ->scalarNode('allow_error_suppressing')->defaultValue(false)->end()
                            ->scalarNode('allow_references')->defaultValue(false)->end()
                            ->scalarNode('allow_casting')->defaultValue(false)->end()
                            ->scalarNode('allow_generators')->defaultValue(false)->end()
                            ->scalarNode('allow_backticks')->defaultValue(false)->end()
                            ->scalarNode('allow_halting')->defaultValue(false)->end()
                            ->scalarNode('allow_aliases')->defaultValue(false)->end()
                            ->arrayNode('whitelisted_functions')->useAttributeAsKey('name')
                                ->prototype('scalar')
                                ->treatNullLike(array())
                                ->end()
                            ->end()
                            ->arrayNode('whitelisted_variables')->useAttributeAsKey('name')
                                ->prototype('scalar')
                                ->treatNullLike(array())
                                ->end()
                            ->end()
                            ->arrayNode('whitelisted_globals')->useAttributeAsKey('name')
                                ->prototype('scalar')
                                ->treatNullLike(array())
                                ->end()
                            ->end()
                            ->arrayNode('whitelisted_super_globals')->useAttributeAsKey('name')
                                ->prototype('scalar')
                                ->treatNullLike(array())
                                ->end()
                            ->end()
                            ->arrayNode('whitelisted_constants')->useAttributeAsKey('name')
                                ->prototype('scalar')
                                ->treatNullLike(array())
                                ->end()
                            ->end()
                             ->arrayNode('whitelisted_magic_constants')->useAttributeAsKey('name')
                                ->prototype('scalar')
                                ->treatNullLike(array())
                                ->end()
                            ->end()
                            ->arrayNode('whitelisted_namespaces')->useAttributeAsKey('name')
                                ->prototype('scalar')
                                ->treatNullLike(array())
                                ->end()
                            ->end()
                            ->arrayNode('whitelisted_aliases')->useAttributeAsKey('name')
                                ->prototype('scalar')
                                ->treatNullLike(array())
                                ->end()
                            ->end()
                            ->arrayNode('whitelisted_uses')->useAttributeAsKey('name')
                                ->prototype('scalar')
                                ->treatNullLike(array())
                                ->end()
                            ->end()
                            ->arrayNode('whitelisted_classes')->useAttributeAsKey('name')
                                ->prototype('scalar')
                                ->treatNullLike(array())
                                ->end()
                            ->end()
                            ->arrayNode('whitelisted_interfaces')->useAttributeAsKey('name')
                                ->prototype('scalar')
                                ->treatNullLike(array())
                                ->end()
                            ->end()
                            ->arrayNode('whitelisted_traits')->useAttributeAsKey('name')
                                ->prototype('scalar')
                                ->treatNullLike(array())
                                ->end()
                            ->end()
                            ->arrayNode('whitelisted_keywords')->useAttributeAsKey('name')
                                ->prototype('scalar')
                                ->treatNullLike(array())
                                ->end()
                            ->end()
                            ->arrayNode('whitelisted_operators')->useAttributeAsKey('name')
                                ->prototype('scalar')
                                ->treatNullLike(array())
                                ->end()
                            ->end()
                            ->arrayNode('whitelisted_types')->useAttributeAsKey('name')
                                ->prototype('scalar')
                                ->treatNullLike(array())
                                ->end()
                            ->end()
                            ->arrayNode('whitelisted_primitives')->useAttributeAsKey('name')
                                ->prototype('scalar')
                                ->treatNullLike(array())
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ->end();

        return $treeBuilder;
    }
}
